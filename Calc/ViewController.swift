//
//  ViewController.swift
//  Calc
//
//  Created by Виталий Крюков on 06.04.2021.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var animationButtomC: UIButton!
    @IBOutlet weak var animationButtomPm: UIButton!
    @IBOutlet weak var animationButtomPr: UIButton!
    @IBOutlet weak var animationButtomDr: UIButton!
    @IBOutlet weak var animationButtom7: UIButton!
    @IBOutlet weak var animationButtom8: UIButton!
    @IBOutlet weak var animationButtom9: UIButton!
    @IBOutlet weak var animationButtomZv: UIButton!
    @IBOutlet weak var animationButtom4: UIButton!
    @IBOutlet weak var animationButtom5: UIButton!
    @IBOutlet weak var animationButtom6: UIButton!
    @IBOutlet weak var animationButtomMi: UIButton!
    @IBOutlet weak var animationButtom1: UIButton!
    @IBOutlet weak var animationButtom2: UIButton!
    @IBOutlet weak var animationButtom3: UIButton!
    @IBOutlet weak var animationButtomPl: UIButton!
    @IBOutlet weak var animationButtom0: UIButton!
    @IBOutlet weak var animationButtomZp: UIButton!
    @IBOutlet weak var animationButtomEq: UIButton!
    

    func animations() {
        animationButtom7.layer.cornerRadius = animationButtom7.frame.size.height / 2
        
        
        
    }
    
    @IBAction func EasyOprations(_ sender: UIButton) {
        operacion = sender.currentTitle!
        memory = current
        isSecond = false
        doubeled = false
    }
    
    
    
    @IBOutlet weak var ResultLabel: UILabel!
    var isFirstOperation = true
    var isSecond = false
    var doubeled = false
    var memory: Double = 0
    var second: Double = 0
    var operacion: String = ""
    var current:Double {
        get {
            return Double(ResultLabel.text!)!
        
        }
        set {
            let value = "\(newValue)"
            ResultLabel.text = "\(newValue)"
            let valueArray = value.components(separatedBy: ".")
            if valueArray.count > 1 && valueArray[1] == "0" {
                ResultLabel.text = valueArray[0]
            } else {
                ResultLabel.text = "\(newValue)"
            }
            isSecond = false
        }
    
    }
    @IBAction func PressKeyNumberBottom(_ sender: UIButton) {
        let n = sender.currentTitle!
        if isSecond {
            if ((ResultLabel.text?.count)! < 15) {
            ResultLabel.text = ResultLabel.text! + n
            }
        }
        else {
            if ((ResultLabel.text?.count)! < 15) {
            ResultLabel.text = n
            isSecond = true
            }
        }
        
    }
    
    @IBAction func EqualPressed(_ sender: UIButton) {
       if isSecond {
            second = current
        
            if isFirstOperation == false {
                switch operacion {
                case "+":
                    current = memory + second
                    isSecond = false
                case "-":
                    current = memory - second
                    isSecond = false
                case "*":
                    current = memory * second
                    isSecond = false
                case "/":
                    current = memory / second
                    isSecond = false
                default:
                    break
                }
            }
       }
      
        isFirstOperation = false
        doubeled = false
    }
    
    
    @IBAction func Clearbottom(_ sender: UIButton) {
        doubeled = false
        isSecond = false
        memory = 0
        second = 0
        operacion = ""
        current = 0
        ResultLabel.text = "0"
        isFirstOperation = true
    }
    @IBAction func PlusMinusbottom(_ sender: UIButton) {
        current = -current
        if ResultLabel.text == "-0" {
            ResultLabel.text = "0"
        }
    }
    
    @IBAction func Percentbottom(_ sender: UIButton) {
        current = current / 100
    }
    
    @IBAction func Dotbottom(_ sender: UIButton) {
        if isSecond && !doubeled {
            ResultLabel.text = ResultLabel.text! + "."
            doubeled = true
        }
        else if !isSecond && !doubeled {
            ResultLabel.text = "0."
        }
    }
    
    override func viewDidLoad() {
        animations()
    }
    
  
}

